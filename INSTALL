  Prerequisites
  -------------

  o Docker Engine
  o Docker Compose

  Setup
  -----

  In order to build this project, you will need
  to install both Docker Engine and Docker Compose
  on a server. For instructions on installing
  Docker Engine, please visit:

  <https://docs.docker.com/install/>

  For instructions on installing Docker Compose,
  please visit:

  <https://docs.docker.com/compose/install/>

  In order to ease deployment to a server, you
  can optionally generate an ssh key pair.

  $ ssh-keygen -t ed25519

  The contents of the public key should be placed
  on the server located in a file called
  .ssh/authorized_keys.

  If you need to transfer from one client machine
  to another, you can copy the contents of the
  private key that was created and move it to
  the new machine.

  $ echo "$SSH_PRIVATE_KEY_CONTENTS" | tr -d '\r' | ssh-add -

  Optionally, you can verify and store the host's
  ssh keys.

  $ mkdir -p ~/.ssh
  $ chmod 700 ~/.ssh
  $ ssh-keyscan ${HOST} >> ~/.ssh/known_hosts

  Build
  -----

  First, clone or download the repository.

  $ git clone https://gitlab.com/Feral2k/media-server.git
  $ cd media-server/server

  Some files necessary to start the server are not
  present within the repository. This is for
  security reasons. You will need to create the
  .passwd-s3fs file and place it inside the server
  directory. The file has the following format:

  BUCKET_NAME:ACCESS_KEY_ID:SECRET_ACCESS_KEY

  The contents of the server directory can then be
  transferred to the host machine.

  $ rsync -hrv . ${USERNAME}@${HOST}:~

  Install
  -------

  The environment variables for some containers
  need to set the UID and GID of the current
  user on the server. In light of that, the
  following instructions all dynamically set
  those environment variables.

  If the containers have already been started on
  the server, then it may be useful to make sure
  that the containers are up to date.

  $ ssh ${USERNAME}@${HOST} PUID=$(id -u) PGID=$(id -g) docker-compose pull

  The containers should then be ready to start.

  $ ssh ${USERNAME}@${HOST} PUID=$(id -u) PGID=$(id -g) docker-compose up -d --build
